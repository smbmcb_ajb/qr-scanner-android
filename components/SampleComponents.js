import { StatusBar } from 'expo-status-bar';
import { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import QRIcon from 'react-native-vector-icons/MaterialIcons'
import React, { Component } from 'react';
import { BarCodeScanner } from 'expo-barcode-scanner';


export default function SampleComponent({navigation}) {
  const myIcon = <Icon name="wifi" size={150} color="black" />;
  const qrIcon = <QRIcon name="qr-code-scanner" size={200} color="black" />
  const [name,setName] = useState('');
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  // const testFetch = ()=>{
  //   console.log('tanga')
  //   // fetch('http://192.168.68.117:8001/users/testApi')
  //   // fetch('http://192.168.68.117:8000/testApi.php',
  //   fetch('http://192.168.68.117:8000/testApi.php',{
  //     method: 'POST',
  //     headers:{
  //       'Content-Type': 'application/json',
  //     },
  //     body: JSON.stringify({
  //       'name': name
  //     })
  //   })
  //     .then(response => response.json())
  //     .then(result => {
  //       console.log(result)
  //       Alert.alert(result.Name)
  //     })
  // }
  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);
const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };
if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
}
if (hasPermission === false) {
    return <Text>No access to camera</Text>;
}

  return (
    <View style={styles.container}>
      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
      {scanned && <Button title={'Tap to Scan Again'} onPress={() => setScanned(false)} />}
    </View>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    
    justifyContent: 'center',
  }
});
