import { StatusBar } from 'expo-status-bar';
import { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import QRIcon from 'react-native-vector-icons/MaterialIcons'

export default function SampleComponent({navigation}) {
    const myIcon = <Icon name="wifi" size={150} color="#3C5393" />;
    const qrIcon = <QRIcon name="qr-code-scanner" size={200} color="#3C5393" />
    const [name,setName] = useState('');
  
    // const testFetch = ()=>{
    //   console.log('tanga')
    //   // fetch('http://192.168.68.117:8001/users/testApi')
    //   // fetch('http://192.168.68.117:8000/testApi.php',
    //   fetch('http://192.168.68.117:8000/testApi.php',{
    //     method: 'POST',
    //     headers:{
    //       'Content-Type': 'application/json',
    //     },
    //     body: JSON.stringify({
    //       'name': name
    //     })
    //   })
    //     .then(response => response.json())
    //     .then(result => {
    //       console.log(result)
    //       Alert.alert(result.Name)
    //     })
    // }
   
  
    return (
      <View style={styles.container}>
          <Image
            source={require('../assets/ma-logo.png')}
            style={{marginTop:18, marginBottom:15}}
          />
          {myIcon}
          <Text style={{fontSize:20, fontWeight:'bold',fontFamily:'Roboto', lineHeight:50}}>Warehouse Traceabitlity</Text>
          <Text style={{fontSize:20, fontWeight:'bold',fontFamily:'Roboto',marginBottom:15}}>QR Scanner</Text>
          {qrIcon}
          {/* <TextInput style={{backgroundColor:'red', width:300,height:50,borderRadius:10}} */}
          {/* onChangeText={setName} */}
          {/* /> */}
        {/* <Text>Hello Sherwin</Text> */}
        <Text
        elevation={5}
          style={styles.button}
          onPress = {()=> navigation.navigate('Sample')}
        >
          <Text>GET STARTED</Text>
        </Text>
        
        <StatusBar style="auto" />
      </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      
      // justifyContent: 'center',
    },
    button: {
      shadowColor: "black",
      shadowOpacity: 0.8,
      shadowRadius: 2,
      shadowOffset: {
        height: 1,
        width: 1
      },
      textAlign: 'center',
      alignSelf: 'center',
      // justifyContent:"center",
      // alignItems: "center",
      borderRadius: 50,
      textAlignVertical: 'center',
      backgroundColor: "#3C5393",
      padding: 10,
      width: '70%',
      height: '10%',
      fontSize: 30,
      color:'white',
      fontWeight:'bold',
      marginTop:25
    },
  });
